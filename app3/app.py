from flask import Flask
import requests

app = Flask(__name__)

@app.route("/")
def home():
    url = 'http://app2:5002'
    txt = ''
    try:
       response = requests.get(url, verify=False)
       if response.status_code == 200:
           txt = response.text
       else:
           txt = 'ответ сервера ' + response.status_code
    except Exception as err:
       txt = 'ошибка ' + str(err)

    return '<html>3' + txt + '</html>'   

if __name__ == "__main__":
    app.run()    