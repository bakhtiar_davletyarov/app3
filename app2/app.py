from flask import Flask
import requests

app = Flask(__name__)

@app.route("/")
def home():
    url = 'http://app1:5001'
    txt = ''
    try:
       response = requests.get(url, timeout=5, verify=False)
       if response.status_code == 200:
           txt = response.text
       else:
           txt = 'ответ сервера ' + response.status_code
    except Exception as err:
       txt = 'ошибка ' + str(err)

    return '<html>2' + txt + '</html>'   

if __name__ == "__main__":
    app.run()    